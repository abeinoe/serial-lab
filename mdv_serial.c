#include "mdv_serial.h"

volatile int STOP=FALSE;

static struct termios oldtio,newtio;

int mdv_serial_init(char *device) {
	int fd;

	fd = open(device, O_RDWR | O_NOCTTY );
	if (fd <0) {
	    return -1;
	}
	tcgetattr(fd,&oldtio);
	bzero(&newtio, sizeof(newtio));

	newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
	newtio.c_iflag = 0;
	newtio.c_oflag = 0;
	newtio.c_lflag = 0;

	newtio.c_cc[VTIME]    = 0;
	newtio.c_cc[VMIN]     = 16;

	tcflush(fd, TCIFLUSH);
	tcsetattr(fd,TCSANOW,&newtio);

	mdv_serial_reset(fd);

	return fd;
}

void mdv_serial_reset(int fd) {
	int i, res;
	unsigned char buf[20];

	while (1) {
		res = read(fd,buf,1);
		if (buf[0]==0x4C) {
			res = read(fd,buf,1);
			if (buf[0]==0x4E) {
				for (i = 0; i < 14; i++) {
					res = read(fd,buf,1);
				}
				break;
			}
		}
	}
}

void mdv_serial_read(int fd, unsigned char *buf) {
	int i, res;

	do {
		res = read(fd, buf, 16);

		if((buf[0] ^ 0x4C) || (buf[1] ^ 0x4E))
			mdv_serial_reset(fd);
		else
			break;
	} while(1);
}

void mdv_serial_write(int fd, const unsigned char *buf) {
	int res;
	res = write(fd,buf,16);
}

void mdv_serial_read_n(int fd, unsigned char *buf, short n) {
	short i;

	for(i=0; i<n; i++) {
		mdv_serial_read(fd, buf);
		buf += 16;
	}
}

void mdv_serial_write_n(int fd, const unsigned char *buf, short n) {
	short i;

	for(i=0; i<n; i++) {
		mdv_serial_write(fd, buf);
		buf += 16;
	}
}
