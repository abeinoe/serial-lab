#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// #define BAUDRATE B2400
#define BAUDRATE B4800
// #define BAUDRATE B9600
// #define BAUDRATE B115200

#define _POSIX_SOURCE 1 /* POSIX compliant source */

#define THIS_FILE		"mdv_serial.h"

#define FALSE 0
#define TRUE 1

int mdv_serial_init(char *device);
void mdv_serial_reset(int fd);
void mdv_serial_read(int fd, unsigned char *buf);
void mdv_serial_write(int fd, const unsigned char *buf);
void mdv_serial_read_n(int fd, unsigned char *buf, short n);
void mdv_serial_write_n(int fd, const unsigned char *buf, short n);
